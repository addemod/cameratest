//
//  CameraViewController.swift
//  CameraTest
//
//  Created by Adam on 2018-11-20.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class CameraViewController: UIImagePickerController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var cameraProtocol: MyCameraProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()

        isModalInPopover = true
        allowsEditing = false
        delegate = self
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            cameraProtocol.onImagePicked(pickedImage)
            dismiss(animated: true, completion: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
