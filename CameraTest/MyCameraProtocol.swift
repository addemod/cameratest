//
//  MyCameraProtocol.swift
//  CameraTest
//
//  Created by Adam on 2018-11-20.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

protocol MyCameraProtocol {
    
    func onImagePicked(_ image: UIImage)
    
}
