//
//  ViewController.swift
//  CameraTest
//
//  Created by Adam on 2018-11-20.
//  Copyright © 2018 Adam. All rights reserved.
//

import UIKit

class ViewController: UIViewController, MyCameraProtocol {

    @IBOutlet weak var imgView: UIImageView!
    
    var cameraController = CameraViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cameraController.cameraProtocol = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func cameraButtonClick(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            cameraController.sourceType = UIImagePickerController.SourceType.camera
            cameraController.allowsEditing = false
        }else{
            cameraController.sourceType = UIImagePickerController.SourceType.photoLibrary
        }
        present(cameraController, animated: true, completion: nil)
    }
    
    func onImagePicked(_ image: UIImage) {
        imgView.contentMode = .scaleAspectFit
        imgView.image = image
        //imgView.reloadInputViews()
    }
}

